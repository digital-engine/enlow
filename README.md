# enlow

用实体，生成代码的工具集合。

- 支持根据实体，生成建表语句。
- 支持根据实体，生成service层代码。
- 支持根据实体，生成controller层代码。
- 支持根据实体，生成自定义代码。

##  快速开始

1. 点击最新的发行版，下载并解压enlow.rar。

2. 修改application-def.yml配置文件：

   ```yaml
   enlow:
     project: D:\xxxx\xxxx\project
     executors:
       - name: EntityResolver
         module: \xxxx\module
         entity: your entity
   
   #    - name: RepositoryGenerator
   #      module: \xxxx\module
   
       - name: TableGenerator
         tablePrefix: your table prefix
   
       - name: ServiceGenerator
         module: \xxxx\module
   
       - name: DtoVoGenerator
         module: \xxxx\module
   
       - name: ControllerGenerator
         module: \xxxx\module
   
   #    - name: VelocityGenerator
   #      template: vm/your template.vm
   #      generatePath: D:\xxxx\xxxx\your file path
   ```
   
3. 双击start-def.bat，运行程序。