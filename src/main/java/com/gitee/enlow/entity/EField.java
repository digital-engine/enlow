package com.gitee.enlow.entity;

import cn.hutool.core.util.StrUtil;
import dotool.java.parser.v1.entity.SAnnotation;
import dotool.java.parser.v1.entity.SField;
import dotool.java.parser.v1.entity.SType;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Setter
@Getter
public class EField extends SField {

    private String preColumn;

    public boolean isTypeMatch(Class<?> clazz) {
        return getType().getClassName().equals(clazz.getName());
    }

    public List<SType> getTypes() {
        List<SType> types = new ArrayList<>(4);
        SType type = getType();
        types.add(type);
        if (type.isGenericType()) {
            types.addAll(type.getTypeArguments());
        }
        return types;
    }

    public boolean isPrimaryKey() {
        return "id".equals(getName());
    }

    public String getColumn() {
        return StrUtil.toUnderlineCase(getName());
    }

    public String getJdbcType() {
        String typeSuffix = isPrimaryKey() ? " unsigned" : "";
        if (isTypeMatch(Integer.class)) {
            return "int" + typeSuffix;

        } else if (isTypeMatch(Long.class)) {
            return "bigint" + typeSuffix;

        } else if (isTypeMatch(String.class)) {
            return "varchar(255)";

        } else if (isTypeMatch(Date.class)) {
            return "timestamp";
        }
        SAnnotation annotation = getAnnotation("com.gitee.dorive.api.annotation.ValueObj");
        if (annotation != null) {
            return "text";
        }
        return null;
    }

    public String getDefault() {
        String name = getName();
        if (isPrimaryKey()) {
            return "auto_increment";

        } else if ("createTime".equals(name)) {
            return "default CURRENT_TIMESTAMP null";

        } else if ("updateTime".equals(name)) {
            return "null on update CURRENT_TIMESTAMP";
        }
        return "null";
    }

    public String getColumnComment() {
        String comment = getComment();
        return StringUtils.isNotBlank(comment) ? "comment '" + comment + "'" : "";
    }

    public boolean isDefaultIndex() {
        return !isPrimaryKey() && getName().endsWith("Id");
    }

    public boolean canIndexed() {
        String jdbcType = getJdbcType();
        return jdbcType != null && !"text".equals(jdbcType);
    }

    public String getIndexName() {
        return getColumn() + (isTypeMatch(String.class) ? "(128)" : "");
    }

    public boolean isEntityDefinition() {
        SAnnotation annotation = getAnnotation("com.gitee.dorive.api.annotation.core.Entity");
        if (annotation != null) {
            return true;
        }
        annotation = getAnnotation("com.gitee.dorive.api.annotation.core.Aggregate");
        return annotation != null;
    }

    public boolean isValueObjDefinition() {
        SAnnotation annotation = getAnnotation("com.gitee.dorive.api.annotation.core.ValueObj");
        return annotation != null;
    }

    public boolean isSerializable() {
        if (isEntityDefinition()) {
            return false;
        }
        if (isValueObjDefinition()) {
            return true;
        }
        SType type = getType();
        if (type.isGenericType()) {
            List<SType> typeArguments = type.getTypeArguments();
            for (SType typeArgument : typeArguments) {
                String className = typeArgument.getClassName();
                if (!className.startsWith("java.lang.") && !className.startsWith("java.util.")) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean isQueryable() {
        if (isEntityDefinition() || isValueObjDefinition()) {
            return false;
        }
        return canIndexed();
    }

    public boolean isEntitySerializable() {
        return isEntityDefinition();
    }

    public boolean isOneEntitySerializable() {
        return isEntitySerializable() && !List.class.getName().equals(getType().getClassName());
    }

    public boolean isManyEntitySerializable() {
        return isEntitySerializable() && List.class.getName().equals(getType().getClassName());
    }

    public String getGenericType() {
        SType type = getType();
        if (type.isGenericType()) {
            type = type.getTypeArguments().get(0);
        }
        return type.toString();
    }

    public String getGenericVoType() {
        return getGenericType() + "Vo";
    }

    public String getEntityVoType() {
        return isManyEntitySerializable() ? "List<" + getGenericVoType() + ">" : getGenericVoType();
    }

    public String getUpperFirst() {
        return StrUtil.upperFirst(getName());
    }

}
