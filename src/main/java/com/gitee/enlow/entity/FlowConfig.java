package com.gitee.enlow.entity;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class FlowConfig {
    private String name;
    private List<Map<String, Object>> executors;
}
