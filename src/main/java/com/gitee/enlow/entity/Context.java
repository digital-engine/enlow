package com.gitee.enlow.entity;

import cn.hutool.core.bean.BeanUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Getter
@Setter
public class Context {
    //========================== 全局共享 ==========================
    // 随机生成包名称
    private String randomPkgName;

    //========================== 不需要解析 ==========================
    // 名称
    private String entityName;
    private String queryName;
    private String repositoryName;
    private String mapperName;
    private String tableName;
    private String serviceName;
    private String controllerName;
    // 包路径
    private String entityPkg;
    private String queryPkg;
    private String repositoryPkg;
    private String mapperPkg;
    private String servicePkg;
    private String controllerPkg;
    private String dtoPkg;
    private String voPkg;
    private String viewPkg;
    // 请求url
    private String controllerUrl;

    //========================== 需要解析 ==========================
    // 实体注释
    private String entityComment;
    // 实体字段
    private List<EField> entityFields = Collections.emptyList();
    // 实体主键类型
    private String entityIdType;
    // 查询对象字段
    private List<EField> queryFields = Collections.emptyList();
    // 引入类型
    private Set<String> importsForSerialization = Collections.emptySet();
    private Set<String> javaImportsForSerialization = Collections.emptySet();
    // 引入类型（Query)
    private Set<String> importsForQuery = Collections.emptySet();
    private Set<String> javaImportsForQuery = Collections.emptySet();
    // 引入类型（ListDto)
    private Set<String> importsForListDto = Collections.emptySet();
    private Set<String> javaImportsForListDto = Collections.emptySet();

    public Context copy() {
        Context context = BeanUtil.copyProperties(this, Context.class);
        context.setImportsForSerialization(new TreeSet<>(context.getImportsForSerialization()));
        context.setJavaImportsForSerialization(new TreeSet<>(context.getJavaImportsForSerialization()));
        context.setImportsForQuery(new TreeSet<>(context.getImportsForQuery()));
        context.setJavaImportsForQuery(new TreeSet<>(context.getJavaImportsForQuery()));
        context.setImportsForListDto(new TreeSet<>(context.getImportsForListDto()));
        context.setJavaImportsForListDto(new TreeSet<>(context.getJavaImportsForListDto()));
        return context;
    }
}
