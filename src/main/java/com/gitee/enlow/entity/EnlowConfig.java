package com.gitee.enlow.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "enlow")
public class EnlowConfig {
    private String project;
    private List<FlowConfig> flows;
}
