package com.gitee.enlow.impl.executor;

import cn.hutool.core.util.StrUtil;
import com.gitee.enlow.api.Executor;
import com.gitee.enlow.entity.Context;
import dotool.module.parser.v1.entity.FileNode;
import dotool.module.parser.v1.entity.Package;
import dotool.module.parser.v1.impl.FileNodeFinder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

@Getter
@Setter
public class ModuleResolver implements Executor {
    private FileNodeFinder fileNodeFinder;
    private String module;
    private String apiModule;
    private String appModule;
    private String domainModule;
    private String infraModule;
    private String entity;
    private String tablePrefix;

    @Override
    public void execute(Context context) {
        if (StringUtils.isNotBlank(module)) {
            if (StringUtils.isBlank(apiModule)) {
                apiModule = module;
            }
            if (StringUtils.isBlank(appModule)) {
                appModule = module;
            }
            if (StringUtils.isBlank(domainModule)) {
                domainModule = module;
            }
            if (StringUtils.isBlank(infraModule)) {
                infraModule = module;
            }
        }
        apiModule = StrUtil.replace(apiModule, File.separator, "/");
        appModule = StrUtil.replace(appModule, File.separator, "/");
        domainModule = StrUtil.replace(domainModule, File.separator, "/");
        infraModule = StrUtil.replace(infraModule, File.separator, "/");

        context.setEntityName(entity);
        context.setQueryName(entity + "Query");
        context.setRepositoryName(entity + "Repository");
        context.setMapperName(StrUtil.upperFirst(tablePrefix) + entity + "Mapper");
        context.setTableName(tablePrefix + "_" + StrUtil.toUnderlineCase(entity));
        context.setServiceName(entity + "Service");
        context.setControllerName(entity + "Controller");

        // 实体包可能存在子包
        FileNode fileNode = fileNodeFinder.findByPattern("**/" + domainModule + "/**/domain/entity/**/" + entity + ".java");
        if (fileNode != null) {
            Package pkg = (Package) fileNodeFinder.findUpperLevel(fileNode);
            context.setEntityPkg(pkg.getQualifier());
        }

        FileNode fileNode1 = fileNodeFinder.findByPattern("**/" + domainModule + "/**/domain/query/");
        if (fileNode1 != null) {
            context.setQueryPkg(((Package) fileNode1).getQualifier());
        }

        FileNode fileNode2 = fileNodeFinder.findByPattern("**/" + domainModule + "/**/domain/repository/");
        if (fileNode2 != null) {
            context.setRepositoryPkg(((Package) fileNode2).getQualifier());
        }

        FileNode fileNode3 = fileNodeFinder.findByPattern("**/" + infraModule + "/**/infra/mapper/");
        if (fileNode3 != null) {
            context.setMapperPkg(((Package) fileNode3).getQualifier());
        }

        FileNode fileNode4 = fileNodeFinder.findByPattern("**/" + appModule + "/**/app/service/");
        if (fileNode4 != null) {
            context.setServicePkg(((Package) fileNode4).getQualifier());
        }

        FileNode fileNode5 = fileNodeFinder.findByPattern("**/" + apiModule + "/**/api/controller/");
        if (fileNode5 != null) {
            context.setControllerPkg(((Package) fileNode5).getQualifier());
        }

        FileNode fileNode6 = fileNodeFinder.findByPattern("**/" + apiModule + "/**/api/dto/");
        if (fileNode6 != null) {
            context.setDtoPkg(((Package) fileNode6).getQualifier() + "." + StrUtil.toUnderlineCase(entity));
        }

        FileNode fileNode7 = fileNodeFinder.findByPattern("**/" + apiModule + "/**/api/vo/");
        if (fileNode7 != null) {
            context.setVoPkg(((Package) fileNode7).getQualifier() + "." + StrUtil.toUnderlineCase(entity));
        }

        FileNode fileNode8 = fileNodeFinder.findByPattern("**/" + apiModule + "/**/api/view/");
        if (fileNode8 != null) {
            context.setViewPkg(((Package) fileNode8).getQualifier() + "." + context.getRandomPkgName());
        }

        context.setControllerUrl(StrUtil.lowerFirst(entity));
    }
}
