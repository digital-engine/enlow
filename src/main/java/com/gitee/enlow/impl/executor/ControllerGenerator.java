package com.gitee.enlow.impl.executor;

import com.gitee.enlow.entity.Context;
import dotool.module.parser.v1.entity.FileNode;
import dotool.module.parser.v1.impl.FileNodeFinder;
import lombok.Getter;
import lombok.Setter;

import java.io.File;

@Getter
@Setter
public class ControllerGenerator extends AbstractGenerator {
    private FileNodeFinder fileNodeFinder;
    private String module;

    @Override
    public void execute(Context context) throws Exception {
        FileNode fileNode = fileNodeFinder.findByPattern("**/" + module + "/**/controller/");
        if (fileNode != null) {
            doGenerate(context, "vm/Controller.vm", fileNode.getPath() + File.separator + context.getControllerName() + ".java");
        }
    }
}
