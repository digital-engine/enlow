package com.gitee.enlow.impl.executor;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.io.FileUtil;
import com.gitee.enlow.api.Executor;
import com.gitee.enlow.entity.Context;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

@Slf4j
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VelocityGenerator implements Executor {

    private String template;
    private String generatePath;

    @Override
    public void execute(Context context) throws Exception {
        String text = generateByTemplate(template, new VelocityContext(BeanUtil.beanToMap(context)));
        FileUtil.writeString(text, generatePath, StandardCharsets.UTF_8);
        log.info("Generated file! path: {}", generatePath);
    }

    private String generateByTemplate(String resource, VelocityContext context) throws IOException {
        Template template = Velocity.getTemplate(resource, "UTF-8");
        StringWriter stringWriter = new StringWriter();
        template.merge(context, stringWriter);
        String text = stringWriter.toString();
        stringWriter.close();
        System.out.println(text);
        return text;
    }

}
