package com.gitee.enlow.impl.executor;

import com.gitee.enlow.entity.Context;
import dotool.module.parser.v1.entity.FileNode;
import dotool.module.parser.v1.impl.FileNodeFinder;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.util.Set;

@Getter
@Setter
public class RepositoryGenerator extends AbstractGenerator {
    private FileNodeFinder fileNodeFinder;
    private String module;

    @Override
    public void execute(Context context) throws Exception {
        FileNode fileNode = fileNodeFinder.findByPattern("**/" + module + "/**/query/");
        FileNode fileNode1 = fileNodeFinder.findByPattern("**/" + module + "/**/repository/");

        if (fileNode != null) {
            context = context.copy();
            Set<String> importsForQuery = context.getImportsForQuery();
            importsForQuery.add("com.gitee.dorive.api.annotation.query.Query");
            importsForQuery.add("lombok.Data");
            doGenerate(context, "vm/Query.vm", fileNode.getPath() + File.separator + context.getQueryName() + ".java");
        }

        if (fileNode1 != null) {
            doGenerate(context, "vm/Repository.vm", fileNode1.getPath() + File.separator + context.getRepositoryName() + ".java");
        }
    }
}
