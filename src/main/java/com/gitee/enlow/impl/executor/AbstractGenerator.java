package com.gitee.enlow.impl.executor;

import com.gitee.enlow.api.Executor;
import com.gitee.enlow.entity.Context;

public abstract class AbstractGenerator implements Executor {

    protected void doGenerate(Context context, String template, String generatePath) throws Exception {
        VelocityGenerator velocityGenerator = new VelocityGenerator();
        velocityGenerator.setTemplate(template);
        velocityGenerator.setGeneratePath(generatePath);
        velocityGenerator.execute(context);
    }

}
