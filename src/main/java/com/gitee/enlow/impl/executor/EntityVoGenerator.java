package com.gitee.enlow.impl.executor;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.gitee.enlow.entity.Context;
import com.gitee.enlow.entity.EField;
import dotool.module.parser.v1.entity.FileNode;
import dotool.module.parser.v1.impl.FileNodeFinder;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class EntityVoGenerator extends AbstractGenerator {
    private FileNodeFinder fileNodeFinder;
    private String module;
    private String filters;

    @Override
    public void execute(Context context) throws Exception {
        FileNode fileNode = fileNodeFinder.findByPattern("**/" + module + "/**/view/");
        if (fileNode != null) {
            String path = fileNode.getPath() + File.separator + context.getRandomPkgName() + File.separator;
            generateEntityVo(context, path);
        }
    }

    private void generateEntityVo(Context context, String path) throws Exception {
        context = context.copy();

        Set<String> importsForSerialization = context.getImportsForSerialization();
        importsForSerialization.add(context.getEntityPkg() + "." + context.getEntityName());
        importsForSerialization.add("lombok.Data");

        List<EField> entityFields = context.getEntityFields();
        EField field = CollUtil.findOne(entityFields, EField::isManyEntitySerializable);
        if (field != null) {
            Set<String> javaImportsForSerialization = context.getJavaImportsForSerialization();
            javaImportsForSerialization.add("java.util.List");
            javaImportsForSerialization.add("java.util.stream.Collectors");
        }

        List<String> entityNames = StrUtil.splitTrim(filters, ",");
        if (entityNames != null && !entityNames.isEmpty()) {
            List<EField> newEntityFields = new ArrayList<>();
            for (EField field1 : entityFields) {
                // 筛除未选取的实体
                if (field1.isEntitySerializable() && !entityNames.contains(field1.getGenericType())) {
                    continue;
                }
                newEntityFields.add(field1);
            }
            context.setEntityFields(newEntityFields);
        }

        doGenerate(context, "vm/EntityVo.vm", path + context.getEntityName() + "Vo.java");
    }
}
