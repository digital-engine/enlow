package com.gitee.enlow.impl.executor;

import cn.hutool.core.util.StrUtil;
import com.gitee.enlow.entity.Context;
import dotool.module.parser.v1.entity.FileNode;
import dotool.module.parser.v1.impl.FileNodeFinder;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.util.Set;

@Getter
@Setter
public class DtoVoGenerator extends AbstractGenerator {
    private FileNodeFinder fileNodeFinder;
    private String module;

    @Override
    public void execute(Context context) throws Exception {
        FileNode fileNode = fileNodeFinder.findByPattern("**/" + module + "/**/dto/");
        FileNode fileNode1 = fileNodeFinder.findByPattern("**/" + module + "/**/vo/");
        String entity = context.getEntityName();
        String packageName = StrUtil.toUnderlineCase(entity);

        if (fileNode != null) {
            String path = fileNode.getPath() + File.separator + packageName + File.separator;
            generateAddDto(context, path);
            generateEditDto(context, path);
            generateListDto(context, path);
            doGenerate(context, "vm/AddBatchDto.vm", path + "AddBatchDto.java");
            doGenerate(context, "vm/EditBatchDto.vm", path + "EditBatchDto.java");
        }

        if (fileNode1 != null) {
            String path = fileNode1.getPath() + File.separator + packageName + File.separator;
            generateListVo(context, path);
        }
    }

    private void generateAddDto(Context context, String path) throws Exception {
        context = context.copy();
        Set<String> importsForSerialization = context.getImportsForSerialization();
        importsForSerialization.add("com.gitee.dorive.core.api.context.Options");
        importsForSerialization.add(context.getEntityPkg() + "." + context.getEntityName());
        importsForSerialization.add("lombok.Data");
        doGenerate(context, "vm/AddDto.vm", path + "AddDto.java");
    }

    private void generateEditDto(Context context, String path) throws Exception {
        context = context.copy();
        Set<String> importsForSerialization = context.getImportsForSerialization();
        importsForSerialization.add("com.gitee.dorive.core.api.context.Options");
        importsForSerialization.add(context.getEntityPkg() + "." + context.getEntityName());
        importsForSerialization.add("lombok.Data");
        Set<String> javaImportsForSerialization = context.getJavaImportsForSerialization();
        javaImportsForSerialization.add("javax.validation.constraints.NotNull");
        doGenerate(context, "vm/EditDto.vm", path + "EditDto.java");
    }

    private void generateListDto(Context context, String path) throws Exception {
        context = context.copy();
        Set<String> importsForListDto = context.getImportsForListDto();
        importsForListDto.add("com.gitee.dorive.core.api.context.Options");
        importsForListDto.add(context.getQueryPkg() + "." + context.getQueryName());
        importsForListDto.add("lombok.Data");
        doGenerate(context, "vm/ListDto.vm", path + "ListDto.java");
    }

    private void generateListVo(Context context, String path) throws Exception {
        context = context.copy();
        Set<String> importsForSerialization = context.getImportsForSerialization();
        importsForSerialization.add(context.getEntityPkg() + "." + context.getEntityName());
        importsForSerialization.add("lombok.Data");
        doGenerate(context, "vm/ListVo.vm", path + "ListVo.java");
    }
}
