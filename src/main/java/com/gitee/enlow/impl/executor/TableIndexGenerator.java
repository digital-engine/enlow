package com.gitee.enlow.impl.executor;

import cn.hutool.core.collection.CollUtil;
import com.gitee.enlow.api.Executor;
import com.gitee.enlow.entity.Context;
import com.gitee.enlow.entity.EField;
import lombok.Getter;
import lombok.Setter;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.StringWriter;
import java.util.List;

@Getter
@Setter
public class TableIndexGenerator implements Executor {

    @Override
    public void execute(Context context) throws Exception {
        String tableName = context.getTableName();
        List<EField> fields = context.getEntityFields();

        fields = (List<EField>) CollUtil.filterNew(fields, f -> f.getJdbcType() != null);
        for (int index = 0; index < fields.size(); index++) {
            EField field = fields.get(index);
            if (index > 0) {
                EField preField = fields.get(index - 1);
                field.setPreColumn(preField.getColumn());
            }
        }

        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("tableName", tableName);
        velocityContext.put("fields", fields);
        Template template = Velocity.getTemplate("vm/TableIndex.vm", "UTF-8");
        StringWriter stringWriter = new StringWriter();
        template.merge(velocityContext, stringWriter);
        String text = stringWriter.toString();
        stringWriter.close();
        System.out.println(text);
    }

}
