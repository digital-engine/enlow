package com.gitee.enlow.impl.executor;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.gitee.enlow.api.Executor;
import com.gitee.enlow.entity.Context;
import com.gitee.enlow.entity.EField;
import dotool.java.parser.v1.entity.SClass;
import dotool.java.parser.v1.entity.SCompilationUnit;
import dotool.java.parser.v1.entity.SField;
import dotool.java.parser.v1.entity.SType;
import dotool.java.parser.v1.impl.JavaScanner;
import dotool.module.parser.v1.entity.FileNode;
import dotool.module.parser.v1.entity.Package;
import dotool.module.parser.v1.impl.FileNodeFinder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Getter
@Setter
public class EntityResolver implements Executor {
    private FileNodeFinder fileNodeFinder;
    private String module;
    private String filters;

    @Override
    public void execute(Context context) throws Exception {
        FileNode fileNode = fileNodeFinder.findByPattern("**/" + module + "/**/entity/**/" + context.getEntityName() + ".java");
        FileNode fileNode1 = fileNodeFinder.findByPattern("**/" + module + "/**/query/**/" + context.getQueryName() + ".java");

        if (fileNode != null) {
            Package pkg = (Package) fileNodeFinder.findUpperLevel(fileNode);
            JavaScanner javaScanner = new JavaScanner();
            SCompilationUnit compilationUnit = javaScanner.parse(pkg.getTypeNames(), fileNode.getFile());
            resolveEntity(context, compilationUnit);
        }

        if (fileNode1 != null) {
            Package pkg = (Package) fileNodeFinder.findUpperLevel(fileNode1);
            JavaScanner javaScanner = new JavaScanner();
            SCompilationUnit compilationUnit = javaScanner.parse(pkg.getTypeNames(), fileNode1.getFile());
            resolveEntityQuery(context, compilationUnit);
        }

        // 处理引入类型
        handleImports(context);
    }

    private void resolveEntity(Context context, SCompilationUnit compilationUnit) {
        SClass sClass = compilationUnit.getSClass();
        context.setEntityComment(sClass.getComment());

        List<SField> sFields = (List<SField>) CollUtil.filterNew(sClass.getFields(), f -> !f.isStatic());
        List<EField> fields = sFields.stream().map(f -> BeanUtil.copyProperties(f, EField.class)).collect(Collectors.toList());
        // 过滤
        if (StringUtils.isNotBlank(filters)) {
            List<String> fieldNames = StrUtil.splitTrim(filters, ",");
            List<EField> newFields = new ArrayList<>(fields.size());
            for (EField field : fields) {
                if (fieldNames.contains(field.getName())) {
                    newFields.add(field);
                }
            }
            fields = newFields;
        }
        context.setEntityFields(fields);

        EField idField = CollUtil.findOne(fields, f -> "id".equals(f.getName()));
        context.setEntityIdType(idField != null ? idField.getType().getClassName() : null);
    }

    private void resolveEntityQuery(Context context, SCompilationUnit compilationUnit) {
        SClass sClass = compilationUnit.getSClass();
        List<SField> sFields = (List<SField>) CollUtil.filterNew(sClass.getFields(), f -> !f.isStatic());
        List<EField> fields = sFields.stream().map(f -> BeanUtil.copyProperties(f, EField.class)).collect(Collectors.toList());
        context.setQueryFields(fields);
    }

    private void handleImports(Context context) {
        handleImportsFromEntity(context);
        handleImportsFromQuery(context);
    }

    private void handleImportsFromEntity(Context context) {
        List<EField> entityFields = context.getEntityFields();
        if (entityFields != null) {
            Set<String> importsForSerialization = new TreeSet<>();
            Set<String> javaImportsForSerialization = new TreeSet<>();
            Set<String> importsForQuery = new TreeSet<>();
            Set<String> javaImportsForQuery = new TreeSet<>();
            for (EField field : entityFields) {
                List<SType> types = field.getTypes();
                if (field.isSerializable()) {
                    addImport(types, importsForSerialization, javaImportsForSerialization);
                }
                if (field.isQueryable()) {
                    addImport(types, importsForQuery, javaImportsForQuery);
                }
            }
            context.setImportsForSerialization(importsForSerialization);
            context.setJavaImportsForSerialization(javaImportsForSerialization);
            context.setImportsForQuery(importsForQuery);
            context.setJavaImportsForQuery(javaImportsForQuery);
        }
    }

    private void handleImportsFromQuery(Context context) {
        List<EField> queryFields = context.getQueryFields();
        if (queryFields != null) {
            Set<String> importsForListDto = new TreeSet<>();
            Set<String> javaImportsForListDto = new TreeSet<>();
            for (EField field : queryFields) {
                List<SType> types = field.getTypes();
                if (field.isQueryable()) {
                    addImport(types, importsForListDto, javaImportsForListDto);
                }
            }
            context.setImportsForListDto(importsForListDto);
            context.setJavaImportsForListDto(javaImportsForListDto);
        }
    }

    private void addImport(List<SType> types, Set<String> imports, Set<String> javaImports) {
        for (SType type : types) {
            String className = type.getClassName();
            if (className.startsWith("java.lang.")) {
                return;
            }
            if (className.startsWith("java.")) {
                javaImports.add(className);
            } else {
                imports.add(className);
            }
        }
    }
}
