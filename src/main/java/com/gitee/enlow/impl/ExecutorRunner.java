package com.gitee.enlow.impl;

import cn.hutool.core.util.RandomUtil;
import com.gitee.enlow.api.Executor;
import com.gitee.enlow.entity.Context;
import com.gitee.enlow.entity.EnlowConfig;
import com.gitee.enlow.entity.FlowConfig;
import dotool.module.parser.v1.entity.Configuration;
import dotool.module.parser.v1.entity.Project;
import dotool.module.parser.v1.impl.ProjectParser;
import lombok.AllArgsConstructor;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.velocity.runtime.resource.loader.FileResourceLoader;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Properties;

@Component
@AllArgsConstructor
public class ExecutorRunner implements ApplicationRunner {

    private final EnlowConfig enlowConfig;
    private final ExecutorFactory executorFactory;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        initVelocity(args);

        Configuration configuration = new Configuration();
        configuration.setExcludeMiddleModules(false);
        configuration.setExcludeMiddlePackages(false);
        configuration.setSortModules(true);
        ProjectParser projectParser = new ProjectParser(configuration);
        Project project = projectParser.parse(enlowConfig.getProject());

        String randomPkgName = "v" + RandomUtil.randomNumbers(4);

        List<FlowConfig> flowConfigs = enlowConfig.getFlows();
        for (FlowConfig flowConfig : flowConfigs) {
            Context context = new Context();
            context.setRandomPkgName(randomPkgName);

            List<Executor> executors = executorFactory.newExecutors(projectParser, project, flowConfig);
            for (Executor executor : executors) {
                executor.execute(context);
            }
        }
    }

    private void initVelocity(ApplicationArguments args) {
        List<String> rootPath = args.getOptionValues("rootPath");
        Properties prop = new Properties();
        if (rootPath == null || rootPath.isEmpty()) {
            prop.put("resource.loader.file.class", ClasspathResourceLoader.class.getName());
        } else {
            prop.put("resource.loader.file.class", FileResourceLoader.class.getName());
            prop.put("resource.loader.file.path", rootPath.get(0));
        }
        Velocity.init(prop);
    }

}
