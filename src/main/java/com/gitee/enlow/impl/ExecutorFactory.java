package com.gitee.enlow.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.gitee.enlow.api.Executor;
import com.gitee.enlow.entity.FlowConfig;
import com.gitee.enlow.impl.executor.*;
import dotool.module.parser.v1.entity.Project;
import dotool.module.parser.v1.impl.FileNodeFinder;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
@AllArgsConstructor
public class ExecutorFactory {

    public static final Map<String, Class<?>> NAME_CLASS_MAP = new LinkedHashMap<>();

    static {
        registry(ModuleResolver.class);
        registry(EntityResolver.class);
        registry(TableGenerator.class);
        registry(TableIndexGenerator.class);
        registry(RepositoryGenerator.class);
        registry(VelocityGenerator.class);
        registry(ServiceGenerator.class);
        registry(ControllerGenerator.class);
        registry(DtoVoGenerator.class);
        registry(EntityVoGenerator.class);
    }

    public static void registry(Class<?> clazz) {
        NAME_CLASS_MAP.put(clazz.getSimpleName(), clazz);
    }

    public List<Executor> newExecutors(FileNodeFinder fileNodeFinder, Project project, FlowConfig flowConfig) throws Exception {
        List<Executor> executors = new ArrayList<>();
        for (Map<String, Object> executorConfig : flowConfig.getExecutors()) {
            executorConfig.put("fileNodeFinder", fileNodeFinder);
            executorConfig.put("Project", project);
            processExecutorConfig(executorConfig);
            String name = (String) executorConfig.get("name");
            String impl = (String) executorConfig.get("impl");
            Class<?> clazz = getClass(name, impl);
            if (clazz != null) {
                Executor executor = (Executor) BeanUtil.copyProperties(executorConfig, clazz);
                executors.add(executor);
            }
        }
        return executors;
    }

    private void processExecutorConfig(Map<String, Object> executorConfig) {
        String module = (String) executorConfig.get("module");
        if (StringUtils.isNotBlank(module)) {
            module = StrUtil.replace(module, File.separator, "/");
            module = StrUtil.removePrefix(module, "/");
            executorConfig.put("module", module);
        }
    }

    private Class<?> getClass(String name, String impl) throws ClassNotFoundException {
        return StringUtils.isNotBlank(impl) ? Class.forName(impl) : NAME_CLASS_MAP.get(name);
    }

}
