package com.gitee.enlow.api;

import com.gitee.enlow.entity.Context;

public interface Executor {

    void execute(Context context) throws Exception;

}
