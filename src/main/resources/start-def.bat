@echo off
title enlow

setlocal enabledelayedexpansion
set file=%~n0
set prefix=start-
set profile=!file:%prefix%=!

echo path: %~dp0
echo file: %~n0
echo profile: %profile%

java -Xms512m -Xmx512m -Dfile.encoding=UTF-8 -jar enlow-1.0.1.jar --rootPath=%~dp0 --spring.profiles.active=%profile% > log.txt

exit